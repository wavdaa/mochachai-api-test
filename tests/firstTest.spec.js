const request = require('supertest');
const express = require('express');
const expect = require('chai').expect;

const app = express();

app.get('/first', (err, res) => {
    res.status(200).json({"ok":"response"});
});

describe('First test', () => {
    it('OK Response', () => {
        request(app)
        .get('/first')
        .end((err, res) => {
            expect(res.statusCode).to.be.equal(200);
        });
    });

    it.skip('Mocky OK Response', (done) => {
        request('https://run.mocky.io')
        .get('/v3/7f4388e8-88a6-41e5-9f72-cb54528746ee')
        .expect(200, done);
    });
});
