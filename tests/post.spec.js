const app = require('../app/src/CourseApp');
const request = require('supertest');
const expect = require('chai').expect;

describe('post requests', () => {
    it('json reponse', () => {
        request(app)
        .post('/course')
        .send({ 'name': 'supertest' })
        .set('Accept', 'application/json')
        .end((err, res) => {
            expect(res.body.name).to.be.equal('supertest');
        });
    });

    it('form reponse', (done) => {
        request(app)
        .post('/course')
        .send('name=supertest')
        .set('Accept', 'application/x-www-form-urlencoded')
        .expect(200, { "id": "2", "name": "supertest" }, done);
    });
});
